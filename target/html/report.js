$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("searchFeature.feature");
formatter.feature({
  "line": 1,
  "name": "search and verify item",
  "description": "",
  "id": "search-and-verify-item",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 3,
  "name": "open amazone.com",
  "description": "",
  "id": "search-and-verify-item;open-amazone.com",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "Browser is opened",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "I navigate to \"http://www.amazon.com/\"",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "I verify page title \"Amazon\" to confirm the site",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "I enter the search key \"Nikon\"",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "I clicked on search button",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I sort results as \"Price: High to Low\"",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I click on second item",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "I verify product topic contains text \"Nikon D3X\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SearchSteps.shouldOpenBrowser()"
});
formatter.result({
  "duration": 8182293414,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://www.amazon.com/",
      "offset": 15
    }
  ],
  "location": "SearchSteps.souldNavigate(String)"
});
formatter.result({
  "duration": 5971448396,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Amazon",
      "offset": 21
    }
  ],
  "location": "SearchSteps.shouldVerifyTitle(String)"
});
formatter.result({
  "duration": 819794715,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Nikon",
      "offset": 24
    }
  ],
  "location": "SearchSteps.shouldEnterSearchKey(String)"
});
formatter.result({
  "duration": 543258797,
  "status": "passed"
});
formatter.match({
  "location": "SearchSteps.shouldClickOnSearchButton()"
});
formatter.result({
  "duration": 6042261861,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Price: High to Low",
      "offset": 19
    }
  ],
  "location": "SearchSteps.shouldSortSearchResult(String)"
});
formatter.result({
  "duration": 1696576915,
  "status": "passed"
});
formatter.match({
  "location": "SearchSteps.shouldClickOnSecondItem()"
});
formatter.result({
  "duration": 2798693182,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Nikon D3X",
      "offset": 38
    }
  ],
  "location": "SearchSteps.shouldVerifyTextOfItemTopic(String)"
});
formatter.result({
  "duration": 8009855501,
  "status": "passed"
});
});